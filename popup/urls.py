from django.conf.urls import url
from popup import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
