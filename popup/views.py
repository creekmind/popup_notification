from django.conf import settings
from django.http import HttpResponse
from django.template import loader, RequestContext

def index(request):
    template = loader.get_template('index.html')
    context = RequestContext(request, {'extjs': settings.EXTJS})
    return HttpResponse(template.render(context))
